# Definitions
# This should work both on *nix and Windows systems, because the libraries
# and cleaning is set conditionally.
CC := gcc
CFLAGS := -Wall -Wextra -pedantic -std=c99
LD := gcc
LFLAGS := 
OBJDIR := obj
SRCS := main.c element.c program.c compiler.c hash.c
OBJS := $(SRCS:%.c=$(OBJDIR)/%.o)
LIBS := 
BIN := alc
DEL := rm -rf $(OBJDIR) $(BIN)
DEBUG := 0

ifeq ($(OS), Windows_NT)
	BIN = alc.exe
	LIBS = -lregex
	DEL = echo Y | del $(BIN) $(OBJDIR)
endif

ifeq ($(DEBUG), 1)
	CFLAGS += -DDEBUG -O0 -g
else
	CFLAGS += -DNDEBUG -O3 -s -finline-functions
	LFLAGS += -s
endif

# Targets
all: $(BIN)

$(BIN): $(OBJDIR) $(OBJS)
	$(LD) $(LFLAGS) -o $(BIN) $(OBJS) $(LIBS)

$(OBJDIR):
	mkdir $(OBJDIR)

$(OBJDIR)/%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	$(DEL)