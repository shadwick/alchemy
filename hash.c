#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "hash.h"

unsigned long _hash(const char *key)
{
  unsigned long hash = 5381;
  int c;
 
  while ((c = *key++))
    hash = ((hash << 5) + hash) + c;

  return hash;
}

HT *ht_create(int size)
{
  HT *ht = malloc(sizeof(HT));
  
  ht_init(ht, size);

  return ht;
}

void ht_init(HT *ht, int size)
{
  ht->size = size;

  ht->tbl = calloc(1, size * sizeof(struct ht_node *));
}

void ht_destroy(HT *ht)
{
  if (!ht) return;

  ht_end(ht);
  free(ht);

  ht = NULL;
}

void ht_end(HT *ht)
{
  if (!ht) return;
  
  ht_clear(ht);
  free(ht->tbl);
}

void *ht_get(HT *ht, const char *key)
{
  if (!ht) return NULL;

  unsigned long idx = _hash(key) % ht->size;

  struct ht_node *n = ht->tbl[idx];
  while (n) {
    if (strncmp(key, n->key, HT_KEY_LEN) == 0)
      return n->val;

    n = n->nxt;
  }

  return NULL;
}

void ht_put(HT *ht, const char *key, void *val)
{
  if (!ht) return;

  unsigned long idx = _hash(key) % ht->size;

  struct ht_node *n_new = calloc(1, sizeof(struct ht_node));
  n_new->val = val;
  strncpy(n_new->key, key, HT_KEY_LEN);
  
  n_new->nxt = ht->tbl[idx];
  ht->tbl[idx] = n_new;
}

void ht_remove(HT *ht, const char *key)
{
  if (!ht) return;

  unsigned long idx = _hash(key) % ht->size;

  struct ht_node *p = NULL, *n = ht->tbl[idx];
  while (n) {
    if (strncmp(key, n->key, HT_KEY_LEN) == 0) {
      if (p)
        p->nxt = n->nxt;
      
      n->key[0] = 0;

      if (ht->tbl[idx] == n)
        ht->tbl[idx] = NULL;

      free (n);
      n = NULL;

      break;
    }

    p = n;
    n = n->nxt;
  }
}

void ht_clear(HT *ht)
{
  if (!ht) return;
  
  int i;
  for (i = 0; i < ht->size; i++) {
    struct ht_node *n = ht->tbl[i];
    while (n) {
      struct ht_node *n_old = n;
      n = n->nxt;
      n_old->key[0] = 0;
      free(n_old);
      n_old = NULL;
    }
    ht->tbl[i] = NULL;
  }
}

struct ht_node *ht_iterate(HT *ht, struct ht_node *prev)
{
    static int idx;
    struct ht_node *n;
    
    if (!prev) {
        idx = 0;
        n = ht->tbl[idx];
    }
    else
        n = prev->nxt;
    
    while (1) {
        if (!n) {
            idx++;
            if (idx < ht->size)
                n = ht->tbl[idx];
            else
                return NULL;
        }
        else {
            if (n->key[0] == 0)
                n = n->nxt;
            else
                break;
        }
    }
    return n;
}

