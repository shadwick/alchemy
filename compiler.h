#ifndef INC_COMPILER_H
#define INC_COMPILER_H

#include "program.h"
#include "hash.h"

/* Bundle compilation progress to pass to compialtion helper functions */
typedef struct {
    Program *program;
    uint16_t code_max, max_elements, max_steps;
    HT element_map;
} CompileState;


int     init    ();
void    cleanup ();
Result  compile (FILE *file, Program *program);

#endif
