#ifndef INC_HASH_H
#define INC_HASH_H

/* simple hashtable
 * David Kaplan <david[at]2of1.org>, 2011
 * Modified since by Erik Shadwick.
 *
 * I found this simple yet functional hash table on 2of1.org in 2011. However,
 * the site appears to be down for now; but either way, as stated above, it
 * was written by David Kaplan originally.
 * I've modified it by adding ht_init, ht_end, and ht_iterate.
 */

#define HT_KEY_LEN  30

struct ht_node {
  void *val;
  char key[HT_KEY_LEN + 1];
  struct ht_node *nxt;
};

typedef struct ht {
  struct ht_node **tbl;
  int size;
} HT;

HT *ht_create(int size);                    /* allocate hashtable mem */
void ht_init(HT *ht, int size);             /* initialize pre-alloc'd HT */
void ht_destroy(HT *ht);                    /* free hashtable mem */
void ht_end(HT *ht);                        /* deinitialize pre-alloc'd HT */
void *ht_get(HT *ht, const char *key);      /* retrieve entry */
void ht_put(HT *ht, const char *key, void *val);  /* store entry */
void ht_remove(HT *ht, const char *key);    /* remove entry */
void ht_clear(HT *ht);
struct ht_node *ht_iterate(HT *ht, struct ht_node *prev);

#endif
