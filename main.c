#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "element.h"
#include "program.h"
#include "compiler.h"
#include "hash.h"

int run(FILE *file) {
    Program *program;
    Result result;
    unsigned int retcode = 0;
    
    /* Initialize and create our program from the input stream, and execute */
    init();
    program = program_create(file, &result);
    if (result == OK) {
        result = program_execute(program);
        if (result != OK) {
            fprintf(stderr, "Line %u: %s\n", program->line_num,
                    result_string(result));
            retcode = 1;
        }
    }
    else {
        fprintf(stderr, "Line %u: %s\n", program->line_num,
                result_string(result));
        retcode = 1;
    }
    if (program)
        program_destroy(program);
    cleanup();
    return retcode;
}


int main(int argc, char *argv[]) {
    /* Read from stdin by default */
    if (argc == 1)
        return run(stdin);
    else {
        if (strcmp(argv[1], "-f") == 0) {
            if (argc > 2) {
                FILE *file;
                unsigned int retcode;
                file = fopen(argv[2], "r");
                if (file != NULL) {
                    retcode = run(file);
                    fclose(file);
                    return retcode;
                }
                fprintf(stderr, "Failed to open '%s'\n", argv[2]);
            }
            else
                fprintf(stderr, "No input file\n");
        }
        else
            fprintf(stderr, "Unrecognized option '%s'\n", argv[1]);
    }
    return 1;
}
