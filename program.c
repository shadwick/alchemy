#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "program.h"
#include "element.h"
#include "compiler.h"
#include "hash.h"


/* ==================== Reaction Actions ==================== */

/* The template for all reaction process functions.
 */
#define REACTION_PROCESS(NAME) \
    FINLINE Result r_##NAME(Reagents *reagents, double *value)

REACTION_PROCESS(calcinate) {
    uint32_t i;
    *value = reagents->elements[0]->value;
    for (i = 1; i < reagents->count; i++)
        *value /= reagents->elements[i]->value;
    return OK;
}

Result r_digest(Reagents *reagents, Element *product) {
    uint32_t i;
    char buf[32];
    if (strcmp(product->name, "Quantifius") == 0) {
        for (i = 0; i < reagents->count; i++) {
            if (fgets(buf, sizeof(buf), stdin))
                element_set_value(reagents->elements[i], strtod(buf, NULL));
        }
    }
    else if (strcmp(product->name, "Inquirium") == 0) {
        for (i = 0; i < reagents->count; i++) {
            buf[0] = getchar();
            element_set_value(reagents->elements[i], (double) buf[0]);
        }
    }
    return OK;
}

REACTION_PROCESS(ferment) {
    uint32_t i;
    *value = reagents->elements[0]->value;
    for (i = 1; i < reagents->count; i++)
        *value -= reagents->elements[i]->value;
    return OK;
}

REACTION_PROCESS(coagulate) {
    uint32_t i;
    *value = 0;
    for (i = 0; i < reagents->count; i++)
        *value += reagents->elements[i]->value;
    *value /= reagents->count;
    return OK;
}

REACTION_PROCESS(fixate) {
    *value = (int64_t) reagents->elements[0]->value;
    return OK;
}

REACTION_PROCESS(cerate) {
    *value = reagents->elements[0]->value - 1;
    return OK;
}

REACTION_PROCESS(distill) {
    uint32_t i;
    int64_t k = reagents->elements[0]->value;
    for (i = 1; i < reagents->count; i++)
        k &= (int64_t) reagents->elements[i]->value;
    *value = k;
    return OK;
}

REACTION_PROCESS(sublimate) {
    uint32_t i;
    int64_t k = reagents->elements[0]->value;
    for (i = 1; i < reagents->count; i++)
        k |= (int64_t) reagents->elements[i]->value;
    *value = k;
    return OK;
}

REACTION_PROCESS(filter) {
    *value = ~((int64_t) reagents->elements[0]->value);
    return OK;
}

REACTION_PROCESS(fuse) {
    uint32_t i;
    *value = 1;
    for (i = 0; i < reagents->count; i++)
        *value *= reagents->elements[i]->value;
    return OK;
}

Result r_multiply(Reagents *reagents, Element *product) {
    uint32_t i;
    if (strcmp(product->name, "Elucidium") == 0) {
        for (i = 0; i < reagents->count; i++)   
            printf("%f\n", reagents->elements[i]->value);
    }
    else if (strcmp(product->name, "Scribius") == 0) {
        for (i = 0; i < reagents->count; i++)
            putchar((char) reagents->elements[i]->value);
    }
    return OK;
}

REACTION_PROCESS(project) {
    uint32_t i;
    *value = 0;
    for (i = 0; i < reagents->count; i++)
        *value += reagents->elements[i]->value;
    return OK;
}



/* ==================== Static Data ==================== */

/* After checking requirements for a reaction, execute the function
   corresponding to the process and save the result.
 */
Result performReaction(Program *program, ReactionProcess process,
                       Reagents *reagents, Element *product) {
    Result result;
    uint32_t needed;
    double value;
    
    /* Check prerequisites */
    needed = reagents_needed_solvent(reagents);
    if (reagents->solvent_amount < needed)
        return EXPL_INSUFFICIENT_SOLVENT;
    if (reagents->solvent_amount > needed)
        return EXPL_EXCESSIVE_SOLVENT;
    if (reagents->solvent == WATER ) {
        if (!reagents_are_soluble(reagents))
            return EXPL_INSOLUBILITY;
        program->has_alkahest = 1;
    }
    if (reagents->solvent == ALKAHEST) {
        if (!program->has_alkahest)
            return EXPL_INSUFFICIENT_ALKAHEST;
        if (needed > ALKAHEST_AMOUNT)
            program->has_alkahest = 0;
    }
    
    /* Dispatch the actual function */
    value = 0;
    switch (process) {
        case CALCINATE: result = r_calcinate(reagents, &value); break;
        case DIGEST:    result = r_digest(reagents, product); break;
        case FERMENT:   result = r_ferment(reagents, &value); break;
        case COAGULATE: result = r_coagulate(reagents, &value); break;
        case FIXATE:    result = r_fixate(reagents, &value); break;
        case CERATE:    result = r_cerate(reagents, &value); break;
        case DISTILL:   result = r_distill(reagents, &value); break;
        case SUBLIMATE: result = r_sublimate(reagents, &value); break;
        case FILTER:    result = r_filter(reagents, &value); break;
        case FUSE:      result = r_fuse(reagents, &value); break;
        case MULTIPLY:  result = r_multiply(reagents, product); break;
        case PROJECT:   result = r_project(reagents, &value); break;
        default:        result = EXPL_UNDEFINED_REACTION; break;
    }
    if (result != OK)
        return result;
    
    /* Assign the resulting value */
    element_set_value(product, value);
    return OK;
}


/* Helper functions to read 16 or 32 bits from the bytecode, and increment the
   program counter accordingly.
 */

FINLINE uint16_t read_int16(Program *program) {
    uint16_t i;
    assert(program);
    i = program->code[program->counter];
    program->counter++;
    return i;
}

FINLINE uint32_t read_int32(Program *program) {
    uint32_t i, m, n;
    assert(program);
    m = program->code[program->counter];
    n = program->code[program->counter + 1];
    program->counter += 2;
    i = (m << 16) | (n & 0xFFFF);
    return i;
}


/* Read the next instruction from the bytecode, and grab the appropriate data
   following it (dependent on that) for execution.
 */
FINLINE Result fetch(Program *program, Instruction *inst) {
    uint16_t i, chunk;
    assert(program);
    assert(inst);
    chunk = read_int16(program);
    program->line_num = chunk >> 1;
    inst->opcode = chunk & 1;
    switch (inst->opcode) {
        case JUMP: {
            chunk = read_int16(program);
            inst->data.j.element = program_get_element(program, chunk);
            chunk = read_int16(program);
            for (i = 0; i < program->num_steps; i++) {
                if (program->steps[i].number == chunk) {
                    inst->data.j.step = &program->steps[i];
                    return OK;
                }
            }
            return EXPL_UNDEFINED_STEP;
        }
        case REACT: {
            uint8_t num_reagents, i;
            Solvent solvent;
            uint16_t amount;
            
            chunk = read_int16(program);
            inst->data.r.process = chunk >> 9;
            solvent = (chunk >> 8) & 1;
            num_reagents = chunk & 0xFF;
            for (i = 0; i < num_reagents; i++) {
                Element *element;
                amount = read_int16(program);
                chunk = read_int16(program);
                element = program_get_element(program, chunk);
                if (inst->data.r.process != DIGEST && !element->initialized)
                    return EXPL_INTANGIBLE_ELEMENT;
                reagents_add_element(&inst->data.r.reagents, element, amount);
            }
            amount = read_int16(program);
            reagents_set_solvent(&inst->data.r.reagents, solvent, amount);
            chunk = read_int16(program);
            inst->data.r.product = program_get_element(program, chunk);
            break;
        }
        default:
            return EXPL_UNDEFINED_INSTRUCTION;
    }
    return OK;
}


/* Carry out the action specified by the Instruction, which has been populated
   by the fetch function.
 */
FINLINE Result execute(Program *program, Instruction *inst) {
    assert(program);
    assert(inst);
    switch (inst->opcode) {
        case JUMP:
            if (inst->data.j.element->value != 0)
                program->counter = inst->data.j.step->offset;
            break;
        case REACT: {
            Result result;
            ReactionProcess process = inst->data.r.process;
            Reagents *reagents = &inst->data.r.reagents;
            Element *product = inst->data.r.product;
            result = performReaction(program, process, reagents, product);
            if (result != OK)
                return result;
            reagents_clear(&inst->data.r.reagents);
        }
    }
    return OK;
}



/* ==================== External Data ==================== */

/* Allocate and return a new Program which has compiled source from the given
 * stream. The Result is set to the compilation outcome.
 * If compilation fails, Result is not OK, the program is still returned.
 */
Program *program_create(FILE *file, Result *result) {
    Program *program;
    assert(file);
    assert(result);
    program = malloc(sizeof(Program));
    program->counter = 0;
    program->has_alkahest = 1;
    *result = compile(file, program);
    if (*result != OK && program->code) {
        free(program->code);
        program->code = NULL;
    }
    return program;
}


/* Deallocate all elements in the program, its compiled bytecode, and it.
 */
void program_destroy(Program *program) {
    assert(program);
    if (program->code)
        free(program->code);
    if (program->elements)
        free(program->elements);
    if (program->steps)
        free(program->steps);
    free(program);
}


Result program_execute(Program *program) {
    Result result = OK;
    Instruction inst;
    assert(program);
    memset(&inst, 0, sizeof(Instruction));
    while (result == OK && program->counter < program->code_size) {
        result = fetch(program, &inst);
        if (result == OK)
            result = execute(program, &inst);
    }
    return result;
}


FINLINE Element *program_get_element(Program *program, uint16_t index) {
    assert(program);
    assert(index < program->num_elements);
    return &program->elements[index];
}


/* Fetch the pretty, readable name for a given Result.
 */
const char *result_string(Result result) {
    switch (result) {
        case OK:
            return "Ok";
        case EXPL_INSUFFICIENT_ALKAHEST:
            return "Insufficient alkahest";
        case EXPL_INSOLUBILITY:
            return "Insoluble reagent(s)";
        case EXPL_EXCESSIVE_SOLVENT:
            return "Excessive solvent";
        case EXPL_INSUFFICIENT_SOLVENT:
            return "Insufficient solvent";
        case EXPL_INTANGIBLE_ELEMENT:
            return "Intangible element";
        case EXPL_UNDEFINED_STEP:
            return "Undefined step";
        case EXPL_UNDEFINED_INSTRUCTION:
            return "Undefined instruction";
        case EXPL_AMBIGUOUS_STEP:
            return "Ambiguous step";
        case EXPL_ERRONEOUS_UNITS:
            return "Erroneous units";
        case EXPL_ALTERING_REALITY:
            return "Altering reality";
        case EXPL_IRRATIONAL_REAGENT:
            return "Irrational reagent";
        case EXPL_REAGENT_SHORTAGE:
            return "Reagent shortage";
        case EXPL_MALFORMED_ELEMENT:
            return "Malformed element";
        case EXPL_UNDEFINED_REACTION:
            return "Undefined reaction";
        case EXPL_SYNTAX_ERROR:
            return "Syntax error";
    }
    return "";
}
